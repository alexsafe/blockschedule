package com.company.alexandru.scheduledblocker.types;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by alexandru on 3/31/2016.
 */
public class ContactInfo implements Serializable{
    String code = null;
    String name = null;
    ArrayList<String> phone_nos = null;
    String startHour = null;
    String endHour = null;
    String typeAction = null;
    String activated = "true";

    public ContactInfo(String code, String name, ArrayList<String> phone_nos, String startHour, String endHour, String typeAction, String activated) {
        super();
        this.code = code;
        this.name = name;
        this.phone_nos = phone_nos;
        this.startHour = startHour;
        this.endHour = endHour;
        this.typeAction = typeAction;
        this.activated = activated;
    }

    public String getActivated() {
        return activated;
    }

    public void setActivated(String activated) {
        this.activated = activated;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getPhone_nos() {
        return phone_nos;
    }

    public void setPhone_nos(ArrayList<String> phone_nos) {
        this.phone_nos = phone_nos;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public String getTypeAction() {
        return typeAction;
    }

    public void setTypeAction(String typeAction) {
        this.typeAction = typeAction;
    }
}
