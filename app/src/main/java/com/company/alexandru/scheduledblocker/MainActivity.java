package com.company.alexandru.scheduledblocker;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.company.alexandru.scheduledblocker.Adapters.ContactsAdapter;
import com.company.alexandru.scheduledblocker.types.ContactInfo;
import com.company.alexandru.scheduledblocker.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String BROADCAST = "android.intent.action.BROADCAST";
    public static final int PICK_CONTACT = 1321;
    public Context ctx;
    public TelephonyManager tm;
    ImageView type;
    ContactsAdapter adapter;
    ListView listView;
    Tracker mTracker;
    ArrayList<ContactInfo> entries;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScheduledApplication application = (ScheduledApplication) getApplication();
        mTracker = application.getDefaultTracker();

        ScheduledApplication.tracker().send(new HitBuilders.EventBuilder("ui", "mainactivity")
                .setLabel("MainScreen")
                .build());
        setContentView(R.layout.activity_main);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.ustom_actiobar, null);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        android.support.v7.app.ActionBar.LayoutParams layoutParams = new android.support.v7.app.ActionBar.LayoutParams(android.app.ActionBar.LayoutParams.MATCH_PARENT,
                android.app.ActionBar.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);

//        addShortcut();
//        if(!getSharedPreferences(Utils.APP_PREFERENCE, Activity.MODE_PRIVATE).getBoolean(Utils.IS_ICON_CREATED, false)){
//            addShortcut();
//            getSharedPreferences(Utils.APP_PREFERENCE, Activity.MODE_PRIVATE).edit().putBoolean(Utils.IS_ICON_CREATED, true);
//        }

        ctx = getApplicationContext();
        Utils utils = new Utils();

//        saveas(RingtoneManager.TYPE_RINGTONE);

        if (!utils.isMyServiceRunning(ScheduledService.class, ctx)) {
            Log.d("test","starting service");
            Intent intent = new Intent(MainActivity.this, ScheduledService.class);
            startService(intent);
        }

        SharedPreferences sharedPref = getSharedPreferences("blocked", 0);
        Set<String> phoneData = sharedPref.getStringSet("set", null);
        String test = sharedPref.getString("test", "nimic");
        //Log.d("test", "phonedata:" + phoneData);
        //Log.d("test", "test:" + test);

        ImageView fab = (ImageView) findViewById(R.id.addButton);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

                startActivityForResult(intent, PICK_CONTACT);

            }
        });

        String fileName = "appData";
        File appDataFile = new File(fileName);


        if (utils.fileExistance(fileName, ctx)) {
            String existingData = utils.readFromFile(fileName, ctx);

            //Log.d("test", "main existingData:" + existingData);
            entries = utils.xmlToList(existingData);
            for (ContactInfo contact : entries) {
                //Log.d("test", "ContactInfocontact :" + contact.getName());
                //Log.d("test", "ContactInfocontact phone:" + contact.getPhone_nos());
            }
// Create the adapter to convert the array to views
            adapter = new ContactsAdapter(this, entries);
// Attach the adapter to a ListView
            listView = (ListView) findViewById(R.id.listContacts);
            listView.setAdapter(adapter);

        }
        // Construct the data source
        ArrayList<ContactInfo> arrayOfUsers = new ArrayList<ContactInfo>();
        for (int i = 0; i < 11; i++) {
            ArrayList<String> phone_nos = new ArrayList<String>();
            for (int j = 0; j < 3; j++) {
                phone_nos.add("+40phone:" + i + " " + j);
            }
            ContactInfo sched = new ContactInfo("123", " gigel vasile cacat " + i, phone_nos, "22:22", "22:22", "silent", "true");
            arrayOfUsers.add(sched);
        }
    }

    public boolean saveas(int type) {
        String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String path = (exStoragePath + "/media/alarms/");
        byte[] buffer = null;
        InputStream fIn = getBaseContext().getResources().openRawResource(
                R.raw.silence);
        int size = 0;

        try {
            size = fIn.available();
            buffer = new byte[size];
            fIn.read(buffer);
            fIn.close();
        } catch (IOException e) {
            return false;
        }


        String filename = "GO.mp3";

        boolean exists = (new File(path)).exists();
        if (!exists) {
            new File(path).mkdirs();
        }

        FileOutputStream save;
        try {
            save = new FileOutputStream(path + filename);
            save.write(buffer);
            save.flush();
            save.close();
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(
                    Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File out=new File("file://"+path+filename+".mp3");
            Uri contentUri = Uri.fromFile(out);
            mediaScanIntent.setData(contentUri);
            this.sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://"+path+filename+".mp3"
                    + Environment.getExternalStorageDirectory())));
        }

        return  true;
    }


    private void addShortcut() {
        //Adding shortcut for MainActivity
        //on Home screen
        Intent shortcutIntent = new Intent(getApplicationContext(),
                MainActivity.class);

        shortcutIntent.setAction(Intent.ACTION_MAIN);

        Intent addIntent = new Intent();
        addIntent
                .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "Scheduled DND");
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                Intent.ShortcutIconResource.fromContext(getApplicationContext(),
                        R.drawable.scheddnd));

        addIntent
                .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("test", "Setting screen name: mainScreen");
        mTracker.setScreenName("mainScreen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
//        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.feedback) {

            ScheduledApplication.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("feedback")
                    .build());

            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("message/rfc822");
            i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"foanapps@gmail.com"});
            i.putExtra(Intent.EXTRA_SUBJECT, "[Scheduled DND] feedback");
            i.putExtra(Intent.EXTRA_TEXT   , "Dear God,");
            try {
                startActivity(Intent.createChooser(i, "Send mail..."));
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        if (id == R.id.help) {
            ScheduledApplication.tracker().send(new HitBuilders.EventBuilder("ui", "open")
                    .setLabel("help")
                    .build());
            Intent helpActivity = new Intent(this, HelpActivity.class);
            startActivity(helpActivity);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        //Log.d("test", "onActivityResult recode:" + reqCode);
        //Log.d("test", "onActivityResult rezcode:" + resultCode);
        //Log.d("test", "onActivityResult data:" + data);
        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    //Log.d("test", "contact data:" + contactData);
//                    Cursor c =  managedQuery(contactData, null, null, null, null);
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    //Log.d("test", "Cursor:" + c);
                    if (c.moveToFirst()) {

                        String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                        String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        //Log.d("test", "contact name:" + name);
                        //Log.d("test", "contact id:" + id);
                        ArrayList<String> numbers = new ArrayList<String>();
                        if (Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                            // Query phone here. Covered next
                            Cursor phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);
                            int i = 1;
                            while (phones.moveToNext()) {
                                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                numbers.add(phoneNumber);
                                //Log.d("test", "Number " + i + " :" + phoneNumber);
                                i++;
                            }
                            phones.close();
                        }
//                        String number = c.getString(c.getColumnIndex(ContactsContract.Contacts.));
                        // TODO Fetch other Contact details as you want to use
                        Intent startAddEdit = new Intent(this, AddEditActivity.class);
                  Bundle extras = new Bundle();
                        extras.putString("send_name", name);
                        extras.putString("send_id", id);
                        extras.putString("action_type", "add");
                        extras.putStringArrayList("send_numbers", numbers);
                        startAddEdit.putExtras(extras);
                        startActivity(startAddEdit);
                    }
                }
                break;
        }
    }

    // broadcast a custom intent.
    public void broadcastIntent(View view) {
        Intent intent = new Intent();
        intent.setAction("com.tutorialspoint.CUSTOM_INTENT");
        sendBroadcast(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Log.d("test","in onItemClick");
        ImageView changeType=(ImageView) view.findViewById(R.id.changeType);
        changeType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.d("test","onItemClick changeType setOnClickListener");
            }
        });
        //Log.d("test","in onItemClick view"+view.findViewById(R.id.changeType));
        ContactInfo contact=entries.get(position);
        entries.remove(contact);
        adapter.notifyDataSetChanged();
    }
}