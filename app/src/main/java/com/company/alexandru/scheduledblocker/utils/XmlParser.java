/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.company.alexandru.scheduledblocker.utils;

import android.util.Log;
import android.util.Xml;

import com.company.alexandru.scheduledblocker.types.ContactInfo;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * This class parses XML feeds from stackoverflow.com. Given an InputStream
 * representation of a feed, it returns a List of entries, where each list
 * element represents a single entry (post) in the XML feed.
 */
public class XmlParser {
    private static final String ns = null;

    // We don't use namespaces

    public ArrayList<ContactInfo> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);
            parser.nextTag();
            return readHs(parser);
            // return readFeed(parser);
        } finally {
            in.close();
        }

    }

    private ArrayList<ContactInfo> readHs(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<ContactInfo> ConcernList = new ArrayList<ContactInfo>();
        int eventType = parser.getEventType();
        //Log.d("test", "in readHs:" + parser.toString());
        ContactInfo currentConcern=null;//new ContactInfo("f","f","f","f","f");
//        currentConcern.setName("asda");

        String prevTag = null;
//        Collection<String> phones=null;
        ArrayList<String> phones = new ArrayList<String>();
        String name = null;
        String contactCode=null;
        String contactName=null;
        String contactSh=null;
        String contactEh=null;
        String activated=null;
        String type = null;
        ArrayList<String> phoneNos=new ArrayList<>();
        String phone=null;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    ConcernList = new ArrayList<ContactInfo>();
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
//                    //Log.d("test", "name START_TAG:" + name);

                    if (name.equals("contact")) {
                        String idContact = parser.getAttributeValue(null, "id");
//                        //Log.d("test", "idContact:" + idContact);
//                        currentConcern.setCode(idContact);
                        contactCode=idContact;
                    }
//                    //Log.d("test", "idContact:" + parser.getText());

//					//Log.d("test", "next2:" + parser.getName());
//					String nameContact=parser.getText();
//					//Log.d("test", "nameContact:" + nameContact);
//					parser.next();
//					//Log.d("test", "next name after:" + parser.getName());
//					String namePhone=parser.getText();


                    if (name.equals("name")) {
//                        //Log.d("test", "name:" + parser.getText());

                    }
                    if (name.contains("phone")) {
//                        //Log.d("test", "phone tag :" + parser.getName());
//                        //Log.d("test", "phone:" + parser.getText());
                    }
                    if (name.equals("startTime")) {
//                        currentConcern.setPhone_nos(phones);
                    }
                    prevTag = name;

//                    break;
                case XmlPullParser.TEXT:

//                    //Log.d("test", "name:" + prevTag);
                    if (prevTag.contains("phone")) {
//                        //Log.d("test", "text:" + parser.getText());
                        if (parser.getText() != null) {
                            phones.add(parser.getText());
//                            phoneNos.add(parser.getText());
                        }
                    }
                    if (prevTag.equals("name")) {
//                        currentConcern.setName(parser.getText());
                        contactName=parser.getText();
                    }
                    if (prevTag.equals("startTime")) {
//                        currentConcern.setStartHour(parser.getText());
                        contactSh=parser.getText();
                    }
                    if (prevTag.equals("activated")) {
//                        currentConcern.setStartHour(parser.getText());
                        activated=parser.getText();
                    }
                    if (prevTag.equals("type")) {
//                        currentConcern.setStartHour(parser.getText());
                        type = parser.getText();
                    }
                    if (prevTag.equals("endTime")) {
//                        currentConcern.setEndHour(parser.getText());
                        contactEh=parser.getText();
                    }
                    break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
//                    //Log.d("test", "name END_TAG:" + name);
                    if (name.equalsIgnoreCase("contact")) {
//                        currentConcern.setPhone_nos(phones);
//                        //Log.d("test", "END CONTACT DATA");
                        currentConcern = new ContactInfo(contactCode, contactName, phones, contactSh, contactEh, type, activated);
                        ConcernList.add(currentConcern);
                        phones=new ArrayList<>();
                        currentConcern = null;

                    }
                    break;

                case XmlPullParser.END_DOCUMENT:
//                    //Log.d("test", "END_DOCUMENT");

            }
            eventType = parser.next();
        }
        for (ContactInfo contact : ConcernList) {
            //Log.d("test", "code: " + contact.getCode());
            //Log.d("test", "name: " + contact.getName());
            //Log.d("test", "activated: " + contact.getActivated());
        }
        return ConcernList;
    }


    private List<ContactInfo> readProperties(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<ContactInfo> ConcernList = new ArrayList<ContactInfo>();
        int eventType = parser.getEventType();
        // ContactInfo currentConcern = null;
        while (eventType != XmlPullParser.END_DOCUMENT) {
            String name = null;
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    ConcernList = new ArrayList<ContactInfo>();
                    break;
                case XmlPullParser.START_TAG:
                    name = parser.getName();
//                    //Log.d("test", "name START_TAG:" + name);
                    if (name == "rows") {
                        // currentConcern = new ContactInfo();
                    }
                    // else if (currentConcern != null)
                {

                    if (name.equals("row")) {
//                        //Log.d("test", "next");
                        parser.nextTag();
                        String idConcern = parser.getAttributeValue(null, "id");
                        String nameConcern = parser.getAttributeValue(null, "name");
                        String markedConcern = parser.getAttributeValue(null, "marked");
//                        //Log.d("test", "id:" + idConcern);
                        //                        //Log.d("test", "name:" + nameConcern);
                        // currentConcern.setId(idConcern);
                        // currentConcern.setName(nameConcern);
//						ContactInfo currentConcern = new ContactInfo(idConcern, nameConcern, markedConcern);
//						ConcernList.add(currentConcern);
                    }
                }
                break;
                case XmlPullParser.END_TAG:
                    name = parser.getName();
//                    //Log.d("test", "name END_TAG:" + name);
                    // if (name.equalsIgnoreCase("rows") && currentConcern != null)
                {
                    // //Log.d("test","if  END_TAG:");
                    // ConcernList.add(currentConcern);
                }
            }
            eventType = parser.next();
        }
        return ConcernList;
    }
}
