package com.company.alexandru.scheduledblocker;

import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.company.alexandru.scheduledblocker.types.ContactInfo;
import com.company.alexandru.scheduledblocker.utils.Utils;
import com.company.alexandru.scheduledblocker.utils.XmlParser;

import org.apache.commons.io.IOUtils;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Calendar;

public class ScheduledService extends Service {
    private static final String TAG = "ScheduledService";
    public TelephonyManager tm;
    public ArrayList<ContactInfo> entries;
    int iniRingerMode;
    int iniVolumeLevel;
    int iniMode;
    int iniVolume;
    AudioManager am;
    int phoneState = 1;
    Uri defaultRintoneUri;
    Ringtone defaultRingtone;
    int counter = 0;
    String miltonKynes = "+441908276907";
    private boolean isRunning = false;
    private Looper looper;

    //    private MyServiceHandler myServiceHandler;
    @Override
    public void onCreate() {
//        HandlerThread handlerthread = new HandlerThread("MyThread", Process.THREAD_PRIORITY_BACKGROUND);
//        handlerthread.start();
//        looper = handlerthread.getLooper();
//        myServiceHandler = new MyServiceHandler(looper);
//        isRunning = true;
        entries = null;
        am = (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);

//        iniMode = am.getMode();
//        iniVolume = am.getStreamVolume(AudioManager.STREAM_MUSIC);


//        am = (AudioManager) getBaseContext().getSystemService(Context.AUDIO_SERVICE);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

//        Mess START_STICKY;age msg = myServiceHandler.obtainMessage();
//        msg.arg1 = startId;
//        myServiceHandler.sendMessage(msg);
        Log.d("test", "My telephony  service started");
//        Toast.makeText(this, "ScheduledService Started.", Toast.LENGTH_SHORT).show();


        tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        tm.listen(new CallStateListener(), PhoneStateListener.LISTEN_CALL_STATE);


        //If service is killed while starting, it restarts.
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        //Log.d("test", "onbind");
        return null;
    }

    @Override
    public void onDestroy() {
        isRunning = false;
        Toast.makeText(this, "ScheduledService Completed or Stopped.", Toast.LENGTH_SHORT).show();
    }

    private void endCall() {
        Context context = getApplicationContext();
        try {
            TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            Class<?> c = Class.forName(telephony.getClass().getName());

            Method m = c.getDeclaredMethod("getITelephony");
            m.setAccessible(true);

//            ITelephony telephonyService = (ITelephony) m.invoke(telephony);
            ITelephony telephonyService = (ITelephony) m.invoke(tm);
//            telephonyService.silenceRinger();
            am.setRingerMode(AudioManager.RINGER_MODE_SILENT);

            // Funciona en 2.2
            // Funciona en 2.3.3
            telephonyService.endCall();
//            telephonyService.
//            //Log.d("test", "ITelepony was used (endCall)");

        } catch (Exception e) {
//            //Log.d("test", "Error ending call: " + e.getMessage());
//            //Log.d("test", "Error ending call", e);
        }
//        String[] bob = {"this", "is", "a", "really", "silly", "list"};
    }

    private class CallStateListener extends PhoneStateListener {




        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            String currTimeHour;
//            setSilent(am,0);
            get_data();

//            defaultRintoneUri = RingtoneManager.getActualDefaultRingtoneUri(getApplicationContext(), RingtoneManager.TYPE_NOTIFICATION);
//            defaultRintoneUri = RingtoneManager.getDefaultUri(RingtoneManager.getDefaultType()
//            defaultRingtone = RingtoneManager.getRingtone(getApplicationContext(), defaultRintoneUri);
//
//            Ringtone r=RingtoneManager.getRingtone(getApplicationContext(), defaultRintoneUri);
//
//            String ringToneName=r.getTitle(getApplicationContext());
//            Log.d("test", "ringtone defaultRintoneUrie:" + defaultRintoneUri);
//            Log.d("test", "ringtone ringtonename:" + ringToneName);
//            //Log.d("test", "phoneState call state:" + phoneState);


//if (phoneState==0){
//    //Log.d("test", "phoneState call silent:");
//    am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//}
            try {
                if (state == TelephonyManager.CALL_STATE_OFFHOOK) {
                    Log.d("test", "telephony CALL_STATE_OFFHOOK");
                }
                if (state == TelephonyManager.CALL_STATE_RINGING) {

                    if (counter == 0) {
                        iniRingerMode = am.getRingerMode();
//                        iniVolumeLevel= am.getStreamVolume(AudioManager.STREAM_MUSIC);
//                        Log.d("test", "iniRingerMode:" + iniRingerMode);
//                        Log.d("test", "telephony iniVolumeLevel:" + iniVolumeLevel);

//                    phoneState = 1;
//                    am.setRingerMode(AudioManager.RINGER_MODE_SILENT);

//                    Log.d("test", "phoneState:" + phoneState+ " counter:"+counter);
//                    if (counter==1)
                        //{// && counter<2) {

//                    iniRingerMode = am.getRingerMode();


//                    //Log.d("test", "managephone Incoming: " + incomingNumber);
//                    if (miltonKynes.equals(incomingNumber) && 1==1) {
//                    if (entries.get(0).getCode().equals("5412")) {
//                    if (Arrays.asList(bob).contains("silly")) {
//                        //Log.d("test", "managephone if dooo it");


                        Calendar c = Calendar.getInstance();
                        int currHour = c.get(Calendar.HOUR_OF_DAY);
                        int currMinute = c.get(Calendar.MINUTE);
                        currTimeHour = String.format("%02d%02d", currHour, currMinute);
                        int rhInt = Integer.parseInt(currTimeHour);

//                        Iterator<ContactInfo> contactsIterator = entries.iterator();
//                        while (contactsIterator.hasNext()) {
//                            ContactInfo contact=contactsIterator.next();
//                        }

                        for (ContactInfo contact : entries) {


//                        //Log.d("test", "ring rt:" + currTimeHour);
                            String contactSh = contact.getStartHour();
                            String contactEh = contact.getEndHour();

//                        //Log.d("test", "ring sh:" + contactSh);
//                        //Log.d("test", "ring eh:" + contactSh);

                            int shInt = Integer.parseInt(contactSh);
                            int ehInt = Integer.parseInt(contactEh);
                            int newEhInt = Integer.parseInt(contactEh);

//                        //Log.d("test", "ring sh:" + shInt);
//                        //Log.d("test", "ring eh:" + ehInt);


                            if (ehInt < shInt) {
                                newEhInt += 2400;
//                            //Log.d("test", "ehInt<shInt: eh:" + ehInt);
//                                if (rhInt < 1200) {
                                if (rhInt < ehInt) {
                                    rhInt += 2400;
//                                //Log.d("test", "si mai mult rhInt<1200 rh:" + rhInt);
                                }
                            }

                            ArrayList<String> phones = contact.getPhone_nos();

//                            Iterator<String> phonesIterator = phones.iterator();
//                            while (phonesIterator.hasNext()) {
//                                String phone=phonesIterator.next();
                            for (String phone : phones) {
//                            //Log.d("test", "testing phone:" + phone);
                                incomingNumber = incomingNumber.replace("+", "");
//                            //Log.d("test", "testing incomingNumber:" + incomingNumber);
                            //Log.d("test", "testing sh:" + shInt + " rh:" + rhInt + " eh:" + ehInt);
                                if (phone.equals(incomingNumber)) {
//                                    Log.d("test", "testing phone:" + contact.getActivated());
                                    if (contact.getActivated().equals("true")) {
                                        Log.d("test", "telephony shInt:" + shInt);
                                        Log.d("test", "telephony rhInt:" + rhInt);
                                        Log.d("test", "telephony ehInt:" + ehInt);
                                        Log.d("test", "telephony newehInt:" + newEhInt);
                                        if (shInt < rhInt && rhInt < newEhInt) {
                                            String action = contact.getTypeAction();
                                            Log.d("test", "telephony action:" + action);
                                            if (action.equals("silent")) {
//                                                am.setStreamVolume(AudioManager.STREAM_MUSIC,0,0);
//                                                am.setStreamVolume(AudioManager.STREAM_RING, 0, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND);

//                                                am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
//                                                am.setStreamMute(AudioManager.STREAM_MUSIC, true);
//                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
//                                                    am.adjustStreamVolume(AudioManager.STREAM_MUSIC, AudioManager.ADJUST_MUTE, 0);
//                                                } else {
//                                                    am.setStreamMute(AudioManager.STREAM_MUSIC, true);
//                                                }
                                                am.setRingerMode(0);
//                                                Log.d("test", "telephony silent:"+am.getStreamVolume(AudioManager.STREAM_MUSIC));
//                                                am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//
//                                                setSilent(am, 0);
                                            }
                                            if (action.equals("vibrate")) {

                                                am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
//                                                Log.d("test", "telephony vibrate:");
                                                am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                                            }
                                            if (action.equals("block")) {

                                                am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                                                endCall();
//                                                Log.d("test", "telephony block:");
                                            }
//                                    //Log.d("test", "managephone testing pune pe silnet");
//                                    silent = true;

                                        }
                                    }
                                }
                            }

                        }

                        counter++;
                    }
//                if (counter==0)

                    Log.d("test", "telephony call counter:" + counter);
                }
                if (state == TelephonyManager.CALL_STATE_IDLE) {
                    if (counter == 1) {

                        am.setRingerMode(iniRingerMode);
//                        try {
//                            RingtoneManager.setActualDefaultRingtoneUri(getApplicationContext(),
//                                    RingtoneManager.TYPE_RINGTONE, defaultRintoneUri);
//                            Log.d("test","ringtone default:"+defaultRintoneUri);
//                        } catch (Throwable t) {
//                            Log.d("test","ringtone Throwable restate:"+t);
//                        }
//                        am.setStreamVolume(AudioManager.STREAM_MUSIC, iniVolumeLevel, 0);
//                        am.setStreamVolume(AudioManager.STREAM_RING, iniVolumeLevel, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND);
//                        am.setMode(iniMode);
//                        am.setStreamVolume(AudioManager.STREAM_RING, iniVolume, AudioManager.FLAG_ALLOW_RINGER_MODES | AudioManager.FLAG_PLAY_SOUND);
//                        //Log.d("test", "end ringer ringer mode:" + am.getRingerMode()); //2=normal, 0=silent, 1=vibrate
//                        phoneState = 0;
                        counter = 0;
                        Log.d("test", "telephony offhook counter:" + counter);
                    }
                }
            } catch (Exception e) {
                // TODO Auto-generated catch block
                Log.d("test", "telephony Exception:" + e);
            }
            super.onCallStateChanged(state, incomingNumber);
        }
    }

    public void setSilent(AudioManager am, int count){
        Context context = getApplicationContext();
        String filename = "GO.mp3";
        String exStoragePath = Environment.getExternalStorageDirectory().getAbsolutePath();
        String path=(exStoragePath +"/media/alarms/");
        File k = new File(path, filename);

        ContentValues values = new ContentValues(4);
        long current = System.currentTimeMillis();
        values.put(MediaStore.MediaColumns.DATA, path + filename  );
        values.put(MediaStore.MediaColumns.TITLE,  filename );
        values.put(MediaStore.Audio.Media.DATE_ADDED, (int) (current / 1000));
        values.put(MediaStore.Audio.Media.MIME_TYPE, "audio/3gpp");

        //new
        values.put(MediaStore.Audio.Media.ARTIST, "cssounds ");
        values.put(MediaStore.Audio.Media.IS_RINGTONE, true);
        values.put(MediaStore.Audio.Media.IS_NOTIFICATION, false);
        values.put(MediaStore.Audio.Media.IS_ALARM, true);
        values.put(MediaStore.Audio.Media.IS_MUSIC, false);

        // Insert it into the database
//        this.getContentResolver()
//                .insert(MediaStore.Audio.Media.getContentUriForPath(k
//                        .getAbsolutePath()), values);

        Uri uri = MediaStore.Audio.Media.getContentUriForPath(k.getAbsolutePath());
        ContentResolver mCr = context.getContentResolver();
        Uri newUri = mCr.insert(uri, values);
        Log.d("test","ringtone newUri:"+newUri);
        try {
            RingtoneManager.setActualDefaultRingtoneUri(context,
                    RingtoneManager.TYPE_RINGTONE, newUri);
//            Settings.System.putString(mCr, Settings.System.RINGTONE,
//                    newUri.toString());
        } catch (Throwable t) {
            Log.d("tes","ringtone Throwable:"+t);
        }
//        try {
//            if (count<=16) {
//                Thread.sleep(500);
//                am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
//                count++;
//                Log.d("test","telephony setSilent count:"+count);
//                setSilent(am, count);
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    public void get_data(){
        String fileName = "appData";
        Utils utils = new Utils();
        if (utils.fileExistance(fileName, this)) {
            String existingData = utils.readFromFile(fileName, this);

            //Log.d("test", "service existingData:" + existingData);
            XmlParser xmlParser = new XmlParser();
            InputStream streamData = null;

            try {
                streamData = IOUtils.toInputStream(existingData, "UTF-8");
                entries = xmlParser.parse(streamData);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

        }
    }
}
//    private final class MyServiceHandler extends Handler {
//        public MyServiceHandler(Looper looper) {
//            super(looper);
//        }
//        @Override
//        public void handleMessage(Message msg) {
//            synchronized (this) {
//                for (int i = 0; i < 10; i++) {
//                    try {
//                        Log.i(TAG, "ScheduledService running...");
//                        Thread.sleep(1000);
//                    } catch (Exception e) {
//                        Log.i(TAG, e.getMessage());
//                    }
//                    if(!isRunning){
//                        break;
//                    }
//                }
//            }
//            //stops the service for the start id.
//            stopSelfResult(msg.arg1);
//        }
//    }
//}