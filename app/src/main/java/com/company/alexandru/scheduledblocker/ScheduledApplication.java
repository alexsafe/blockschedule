package com.company.alexandru.scheduledblocker;

import android.app.Application;

import com.company.alexandru.scheduledblocker.R;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by alexandru on 5/12/2016.
 */
public class ScheduledApplication extends Application {
    private static Tracker mTracker;
    private static ScheduledApplication mInstance;
    private static GoogleAnalytics analytics;

    @SuppressWarnings("unused") // Method is unused in codebase; kept here for reference.
    public static GoogleAnalytics analytics() {
        return analytics;
    }

    public static Tracker tracker() {
        return mTracker;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        analytics = GoogleAnalytics.getInstance(this);

        // TODO: Replace the tracker-id with your app one from https://www.google.com/analytics/web/
        mTracker = analytics.newTracker("UA-77681950-1");

        // Provide unhandled exceptions reports. Do that first after creating the tracker
        mTracker.enableExceptionReporting(true);

        // Enable Remarketing, Demographics & Interests reports
        // https://developers.google.com/analytics/devguides/collection/android/display-features
        mTracker.enableAdvertisingIdCollection(true);

        // Enable automatic activity tracking for your app
        mTracker.enableAutoActivityTracking(true);

        mTracker.send(new HitBuilders.ScreenViewBuilder().setCustomDimension(1, null).build());
    }

    public static ScheduledApplication getInstance() { return mInstance; }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
        }
        return mTracker;
    }
}