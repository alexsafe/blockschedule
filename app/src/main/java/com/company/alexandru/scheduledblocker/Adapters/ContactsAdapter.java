package com.company.alexandru.scheduledblocker.Adapters;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.company.alexandru.scheduledblocker.AddEditActivity;
import com.company.alexandru.scheduledblocker.ScheduledService;
import com.company.alexandru.scheduledblocker.R;
import com.company.alexandru.scheduledblocker.ScheduledApplication;
import com.company.alexandru.scheduledblocker.types.ContactInfo;
import com.company.alexandru.scheduledblocker.utils.Utils;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

/**
 * Created by alexandru on 4/4/2016.
 */
public class ContactsAdapter extends BaseAdapter {

    public static int noOfClicks = 0;
    Utils utils = new Utils();
    //    ArrayList<Integer> clicks;
    int[] clicks;
    ArrayList<ContactInfo> liName;
    Context con;
    TextView name;
    TextView fromTo;
    Switch mySwitch;
    ImageView changeType;
    ContactsAdapter adapter;
    ContactInfo ContactInfo;
    String type;
    Tracker mTracker;


    public ContactsAdapter(Context context, ArrayList<ContactInfo> ContactInfos) {
//        super(context, 0, ContactInfos);
//        this.liName=new ArrayList<ContactInfo>();
//        this.liName.addAll(ContactInfos);
        this.liName = ContactInfos;
        this.con = context;
        int size = ContactInfos.size();

        //Log.d("test", "noOfclicks pt pozitions:" + size);
        clicks = new int[size];


        ScheduledApplication application = ScheduledApplication.getInstance();
        mTracker = application.getDefaultTracker();
//
//        mTracker = application.getTracker(TrackerName.APP_TRACKER);
        adapter = this;
    }

    @Override
    public int getCount() {
        return liName.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ContactInfo = liName.get(position);
        ViewHolder viewholder = null;
        //Log.d("test", "getViewcontact name:" + ContactInfo.getName());
        //Log.d("test", "getViewcontact type:" + ContactInfo.getTypeAction());
        //Log.d("test", "getViewcontact activated:" + ContactInfo.getActivated());

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(con).inflate(R.layout.item_contact, parent, false);


            /** if not or view is newly inflated create convert view */
//        ViewHolder viewholder = new ViewHolder(convertView, position, getCount(), ContactInfo, con, liName, adapter);
            viewholder = new ViewHolder();


//        Drawable voucherImage = createBarCode(voucherCode);/** get it from as you want */
//        viewholder.ivBarCode.setImageDrawable(voucherImage);


            /** set tag to new view */
//        convertView.setTag(voucherCode);

            /** return view */
//
            viewholder.name = (TextView) convertView.findViewById(R.id.contactName);
            viewholder.fromTo = (TextView) convertView.findViewById(R.id.fromTo);
            viewholder.mySwitch = (Switch) convertView.findViewById(R.id.activated);
            viewholder.changeType = (ImageView) convertView.findViewById(R.id.changeType);
            if (!ContactInfo.getCode().equals("test")) {
//            name.setText(ContactInfo.getName());

                /**  set data to view */
                viewholder.name.setText(ContactInfo.getName());
                String startHour = formatHout(ContactInfo.getStartHour());
                String endHour = formatHout(ContactInfo.getEndHour());

                //Log.d("test", "sh:" + startHour);

//            fromTo.setText(ContactInfo.getStartHour() + " -> " + ContactInfo.getEndHour());
                viewholder.fromTo.setText(startHour + " - " + endHour);

//        set the switch to ON
                if (ContactInfo.getActivated().equals("true"))
                    viewholder.mySwitch.setChecked(true);
                else
                    viewholder.mySwitch.setChecked(false);
                /*
                if (ContactInfo.getTypeAction().equals("silent")) {
//                    viewholder.changeType.setBackgroundResource(android.R.drawable.ic_lock_silent_mode);
                    viewholder.changeType.setBackgroundResource(R.drawable.mute);
                }
                */
                if (ContactInfo.getTypeAction().equals("block")) {
//                    viewholder.changeType.setBackgroundResource(android.R.drawable.ic_lock_silent_mode);
                    viewholder.changeType.setBackgroundResource(R.drawable.no_entry);
                }
                if (ContactInfo.getTypeAction().equals("vibrate")) {
//                    viewholder.changeType.setBackgroundResource(android.R.drawable.ic_lock_silent_mode);
                    viewholder.changeType.setBackgroundResource(R.drawable.vibrate);
                }
            }
            final ContactInfo currentContact = liName.get(position);
            utils = new Utils();
            name = (TextView) convertView.findViewById(R.id.contactName);
            fromTo = (TextView) convertView.findViewById(R.id.fromTo);
            mySwitch = (Switch) convertView.findViewById(R.id.activated);
            changeType = (ImageView) convertView.findViewById(R.id.changeType);
            clicks[position] = 0;
            viewholder.changeType.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

//                    Log.d("test", "click" + position);
                    clicks[position]++;
                    ScheduledApplication.tracker().send(new HitBuilders.EventBuilder("ui", "main")
                            .setLabel("Main")
                            .setAction("changeType " + clicks[position])
                            .build());
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("edit")
                            .setAction("changeType")
                            .setLabel("Main")
                            .build());
                    if (clicks[position] == 1) {
                        //Log.d("test", "1:");
//                        v.setBackgroundResource(android.R.drawable.stat_notify_call_mute);
                        v.setBackgroundResource(R.drawable.no_entry);
                        type="block";
                    }
                    if (clicks[position] == 2) {
//                        v.setBackgroundResource(android.R.drawable.stat_notify_chat);
                        type="vibrate";
                        v.setBackgroundResource(R.drawable.vibrate);
                        clicks[position] = 0;
                    }
                    /*
                    if (clicks[position] == 3) {
                        type="silent";
//                        v.setBackgroundResource(android.R.drawable.ic_lock_silent_mode);
                        v.setBackgroundResource(R.drawable.mute);


                        clicks[position] = 0;
                    }*/
//                    currentContact.setTypeAction(type);
//                    utils.modifyActionType(con, currentContact);
                    utils.modifyActionType(currentContact.getCode(), type, con);
                    startService();
                }
            });
//            viewholder.mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView,
//                                             boolean isChecked) {
//                    //Log.d("test", "onCheckedChanged");
//                    mySwitch.setChecked(isChecked);
//                    utils.modifyActivated(currentContact.getCode(), isChecked, con);
//                }
//            });
            viewholder.mySwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean thisck = ((Switch) v).isChecked();
                    ScheduledApplication.tracker().send(new HitBuilders.EventBuilder("ui", "main")
                            .setLabel("main")
                            .setAction("mySwitch "+thisck)
                            .build());
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("edit")
                            .setAction("mySwitch")
                            .setLabel("main")
                            .build());
                    //Log.d("test", "onCheckedChanged:" + thisck);
                    mySwitch.setChecked(thisck);
                    utils.modifyActivated(currentContact.getCode(), thisck, con);
                    startService();
                }
            });
            viewholder.fromTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Log.d("test", "contact selected:" + currentContact.getName());
                    //Log.d("test", "contact selected:" + currentContact.getCode());
                    //Log.d("test", "contact selected:" + currentContact.getStartHour());
                    //Log.d("test", "contact selected:" + currentContact.getEndHour());

                    ScheduledApplication.tracker().send(new HitBuilders.EventBuilder("ui", "main")
                            .setLabel("Main")
                            .setAction("fromTo")
                            .build());
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("edit")
                            .setAction("fromTo")
                            .setLabel("Main")
                            .build());
                    Intent startAddEdit = new Intent(con, AddEditActivity.class);
//                    Bundle extras = new Bundle();
//                    extras.putString("send_name", mContactInfo.getName());
//                    extras.putString("send_id", mContactInfo.getCode());
//                    extras.putString("action_type", "edit");
                    startAddEdit.putExtra("mContactInfo", currentContact);
                    startAddEdit.putExtra("action_type", "edit");

//                    extras.putStringArrayList("send_numbers", mContactInfo.getPhone_nos());
//                    startAddEdit.putExtras(extras);
//                    con.startActivity(startAddEdit);
                    ((Activity) con).startActivityForResult(startAddEdit, 69);
                    startService();
                }
            });
            viewholder.name.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(final View v) {
                    //Log.d("test", "on long click press");

                    ScheduledApplication.tracker().send(new HitBuilders.EventBuilder("ui", "main")
                            .setLabel("Main")
                            .setAction("delete")
                            .build());
                    mTracker.send(new HitBuilders.EventBuilder()
                            .setCategory("edit")
                            .setAction("delete")
                            .setLabel("Main")
                            .build());
                    new AlertDialog.Builder(con)
                            .setTitle("Delete")
                            .setMessage("Do you really want to delete the contact from this list?")
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //Log.d("test", "in delete aproape");
//                                    ContactInfo contact=mContactAdapter.getItem(mPosition);
                                    utils.deleteContact(currentContact.getCode(), con);
                                    liName.remove(position);
//                                    mContactAdapter = new ContactsAdapter(mContext,mLiName);

//                                    v.setVisibility(View.GONE);
                                    adapter.notifyDataSetChanged();
//                                    notifyDataSetChanged();
                                }
                            })
                            .setNegativeButton(android.R.string.no, null).show();
                    startService();
                    return false;
                }
            });


        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }

        return convertView;
    }

    private void startService() {
//        Utils utils = new Utils();
        Intent intent = new Intent(con, ScheduledService.class);
//        con.startService(intent);
        //Log.d("test", "My service started 2");
    }

    private String formatHout(String hour) {
        //Log.d("test", "hour:" + hour);
        String[] shArray = hour.split("");
        int shLen = hour.length();
        //Log.d("test", "shen:" + shLen);
//            for (int i=0;i<=shLen;i++){
//                //Log.d("test", "sharray:" + shArray[i]);
//            }
        if (shLen == 1) {
            hour = "00" + ":0" + shArray[1];
        }
        if (shLen == 2) {
            hour = "00:" + shArray[1] + shArray[2];
        }
        if (shLen == 3) {
            hour = "0" + shArray[1] + ":" + shArray[2] + shArray[3];
        }
        if (shLen == 4) {
//                //Log.d("test","sh;eng e 4");
//                //Log.d("test","sh:"+shArray[0]+" "+shArray[1]+":"+shArray[2]+" "+shArray[3]+" "+shArray[4]);
            hour = shArray[1] + shArray[2] + ":" + shArray[3] + shArray[4];
        }
        return hour;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode) {
            case 90:
                if (resultCode == 69) {
                    Bundle res = data.getExtras();
                    String result = res.getString("param_result");
                    //Log.d("FIRST", "result:"+result);
                }
                break;
        }
    }

    /**
     * static cache item view
     */
    static class ViewHolder {
        TextView name;
        TextView fromTo;
        Switch mySwitch;
        ImageView changeType;
        int mPosition;
        int[] clicks;
        Utils utils;
        ContactInfo mContactInfo;
        ContactInfo currentContact;
        Context mContext;
        ArrayList<ContactInfo> mLiName;
        ContactsAdapter mContactAdapter;

        /**
         * holder constructor
         */
//        public ViewHolder(View view, int position, int size, ContactInfo contactInfo, Context context, ArrayList<ContactInfo> liName, final ContactsAdapter adapter) {
//        }

    }


}

