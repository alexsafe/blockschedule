package com.company.alexandru.scheduledblocker;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TimePicker;

import com.company.alexandru.scheduledblocker.types.ContactInfo;
import com.company.alexandru.scheduledblocker.utils.Utils;
import com.company.alexandru.scheduledblocker.utils.XmlParser;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.apache.commons.io.IOUtils;
import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class AddEditActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;
    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    public String startTime;
    public String endTime;
    public String name;
    public Set<String> contactData;
    public ContactInfo contactInfo;
    String actionType;
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };
    Tracker mTracker;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_edit);
        ScheduledApplication application = (ScheduledApplication) getApplication();
        mTracker = application.getDefaultTracker();
        ScheduledApplication.tracker().send(new HitBuilders.EventBuilder("ui", "addedit")
                .setLabel("AddEdit")
                .build());
        mTracker.setScreenName("addeditscren");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
//        contactData= new ArraySet<String>();
        contactData = new HashSet<String>();
        Intent i = getIntent();

        contactInfo = new ContactInfo("", "", null, "", "", "", "");
//        Log.d("test", "addedit contact info null:" + contactInfo);
        Bundle extras = new Bundle();
        actionType = getIntent().getExtras().getString("action_type");
//        Log.d("test", "addedit contact actionType:" + actionType);
        if (actionType.equals("add")) {

            name = extras.getString("send_name");
//            Log.d("test", "full sent name:" + getIntent().getExtras().getString("send_name"));
//            Log.d("test", "full sent id:" + getIntent().getExtras().getString("send_id"));
//            Log.d("test", "full sent numbers:" + getIntent().getExtras().getStringArrayList("send_numbers"));
            ArrayList<String> numbers = getIntent().getExtras().getStringArrayList("send_numbers");


            contactData.add(getIntent().getExtras().getString("send_name"));
            contactInfo.setCode(getIntent().getExtras().getString("send_id"));
            contactInfo.setName(getIntent().getExtras().getString("send_name"));
            contactInfo.setPhone_nos(numbers);
            contactInfo.setTypeAction("block");

            contactInfo.setActivated("true");
            contactData.addAll(numbers);
        }
        else
        {
            contactInfo = (ContactInfo) i.getSerializableExtra("mContactInfo");
//            Log.d("test", "addedit contact info:" + contactInfo);
        }
//        if (actionType.equals("edit")) {
//            contactInfo.setActivated(getIntent().getExtras().getString("activated"));
//        }



//        Log.d("test", "contactinfo1:" + contactInfo);
        DialogFragment newFragment = new StartPickerFragment(contactData, contactInfo, actionType);
//        DialogFragment newFragment = new StartPickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);


        // Set up the user interaction to manually show or hide the system UI.
        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        // Upon interacting with UI controls, delay any scheduled hide()
        // operations to prevent the jarring behavior of controls going away
        // while interacting with the UI.
//        findViewById(R.id.dummy_button).setOnTouchListener(mDelayHideTouchListener);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
        delayedHide(100);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            // This ID represents the Home or Up button.
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in [delay] milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    public static class StartPickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        Set<String> set = new HashSet<String>();
        ContactInfo mContactInfo;//=new ContactInfo();
        String mActionType;
        public StartPickerFragment(Set<String> btn, ContactInfo contactInfo, String actionType) {
//            Log.d("test", "btn:" + btn);
            set = btn;
            mActionType=actionType;
            mContactInfo = contactInfo;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            TimePickerDialog startDialog = new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
            startDialog.setTitle("Choose start");
            // Create a new instance of TimePickerDialog and return it
            return startDialog;
        }

//    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//        // Do something with the time chosen by the user
//    }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//            Log.d("test", " start time set view:" + view);
//            Log.d("test", " start time set hourOfDay:" + hourOfDay);
//            Log.d("test", " start time set minute:" + minute);
            String startHour = String.format("%02d%02d", hourOfDay, minute);
            int shInt = Integer.parseInt(startHour);
            startHour = Integer.toString(shInt);
//            String startHour = hourOfDay + ":" + minute;
            set.add(startHour);

            mContactInfo.setStartHour(startHour);
//            Log.d("test", "contactinfo2:" + mContactInfo);
            DialogFragment newFragment = new EndPickerFragment(set, mContactInfo, mActionType);
            newFragment.show(getFragmentManager(), "timePicker");

        }
    }

    public static class EndPickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {
        Set<String> set = new HashSet<String>();
        ContactInfo mcontactInfo;//=new ContactInfo();
        String mActionType;

        public EndPickerFragment(Set<String> btn, ContactInfo contactInfo, String actionType) {
//            Log.d("test", "bt EndPickerFragmentn:" + btn);
            set = btn;
            mActionType=actionType;
            mcontactInfo = contactInfo;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
//            Calendar.H
            int minute = c.get(Calendar.MINUTE);
            TimePickerDialog endDialog = new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
            endDialog.setTitle("Choose end");
            // Create a new instance of TimePickerDialog and return it
            return endDialog;
        }

//    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//        // Do something with the time chosen by the user
//    }

        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
//            Log.d("test", " end time set view:" + view);
//            Log.d("test", " end time set hourOfDay:" + hourOfDay);
//            Log.d("test", " end time set minute:" + minute);
//            String endHour = hourOfDay + ":" + minute;
            String endHour = String.format("%02d%02d", hourOfDay, minute);
            int ehInt = Integer.parseInt(endHour);
            endHour = Integer.toString(ehInt);
            set.add(endHour);
            mcontactInfo.setEndHour(endHour);
//            Log.d("test", "contactinfo3:" + mcontactInfo.getName());
//            Log.d("test", "contactinfo3:" + mcontactInfo.getPhone_nos());
//            Log.d("test", "contactinfo3:" + mcontactInfo.getStartHour());
//            name="asda";
//            contactData.add(startTime);

//            contactData.add(endTime);

                WriteToXMLTask writeTask = new WriteToXMLTask(getActivity(),mActionType);
                writeTask.execute(mcontactInfo);



        }
    }

    public static class WriteToXMLTask extends AsyncTask<ContactInfo, Void, ContactInfo> {
        String mActionType;
        private Context context;

        public WriteToXMLTask(Context ctx, String actionType) {
            context = ctx;
            mActionType=actionType;
        }

        @Override
        protected ContactInfo doInBackground(ContactInfo... params) {
//            Log.d("test", "doInBackground:" + params[0]);
//            Log.d("test", "doInBackground:" + params);
//            Log.d("test", "doInBackground mActionType:" + mActionType);

            ContactInfo contact = params[0];
            Utils utils = new Utils();
            String fileName = "appData";
            File appDataFile = new File(fileName);
//        if (!appDataFile.exists())
            ArrayList<ContactInfo> entries = new ArrayList<ContactInfo>();
            if (!utils.fileExistance(fileName, context)) {
//                Log.d("test", "doInBackground if:" );
                entries.add(contact);
                if (mActionType.equals("add")){
                    utils.createFile(context, entries, fileName);
                }
                if (mActionType.equals("edit")){
                    utils.modifyHours(context, contact);
                }

            } else {
//                Log.d("test", "doInBackground else:" );
                String existingData = utils.readFromFile(fileName, context);
//                Log.d("test", "fileName:" + existingData);
                XmlParser xmlParser = new XmlParser();
//        InputStream streamData = new ByteArrayInputStream(existingData.getBytes(StandardCharsets.UTF_8));
                InputStream streamData = null;
                try {
                    streamData = IOUtils.toInputStream(existingData, "UTF-8");


                    try {
                        entries = xmlParser.parse(streamData);
                        entries.add(contact);
//                        utils.createFile(context, entries, fileName);
                        if (mActionType.equals("add")){
                            utils.createFile(context, entries, fileName);
                        }
                        if (mActionType.equals("edit")){
                            utils.modifyHours(context, contact);
                        }

                    } catch (XmlPullParserException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
            return params[0];

        }

        @Override
        protected void onPostExecute(ContactInfo result) {
            Log.d("test", "onPostExecute:" + result);

            Intent backToMain = new Intent(context, MainActivity.class);
//            Bundle extras = new Bundle();
//            extras.putString("send_name", name);
//            extras.putString("send_id", id);
//            extras.putStringArrayList("send_numbers",numbers);
//            extras.putStringArray("asdad",set);
//            startAddEdit.putExtras(extras);
            context.startActivity(backToMain);
        }

//        @Override
//        protected ContactInfo doInBackground(String... urls)
//        {
//            Log.d("test","in doInBackground:"+urls);
////            return Util.loadXmlFromNetwork(urls[0]);
//            return null;
//        }
    }
}


