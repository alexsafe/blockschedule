package com.company.alexandru.scheduledblocker;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.company.alexandru.scheduledblocker.utils.UrlEngine;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

public class HelpActivity extends AppCompatActivity {
    ConnectionDetector cd;
    SharedPreferences prefs;
    Tracker mTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScheduledApplication application = (ScheduledApplication) getApplication();
        mTracker = application.getDefaultTracker();

        setContentView(R.layout.activity_help);

        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayShowHomeEnabled(false);
        mActionBar.setDisplayShowTitleEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actiobar2, null);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        android.support.v7.app.ActionBar.LayoutParams layoutParams = new android.support.v7.app.ActionBar.LayoutParams(android.app.ActionBar.LayoutParams.MATCH_PARENT,
                android.app.ActionBar.LayoutParams.MATCH_PARENT);
        mActionBar.setCustomView(mCustomView, layoutParams);
        Toolbar parent = (Toolbar) mCustomView.getParent();
        parent.setContentInsetsAbsolute(0, 0);
        
        prefs = getSharedPreferences("ig-user", 0);
        String in_shared  = prefs.getString("in_shared", "false"); 

        cd = new ConnectionDetector(getApplicationContext());

        if (cd.isConnectingToInternet()) {
            UrlEngine.getAppData(HelpActivity.this);
//            SharedPreferences sharedpreferences = this.getApplicationContext().getSharedPreferences("ig-user", 0);
//            jokeTxt = sharedpreferences.getString("joke", null);
        }
        else if (in_shared.equals("true")){
            TextView introTxt = (TextView) findViewById(R.id.intro);
            TextView plusTxt = (TextView) findViewById(R.id.plus);
            TextView nameTxt = (TextView) findViewById(R.id.name);
            TextView intervalTxt = (TextView) findViewById(R.id.intervalTxt);
            TextView switchTxt = (TextView) findViewById(R.id.switchTxt);
            TextView muteTxt = (TextView) findViewById(R.id.muteTxt);
            TextView vibrateTxt = (TextView) findViewById(R.id.vibrateTxt);
            TextView blockTxt = (TextView) findViewById(R.id.blockTxt);
            TextView tldrTxt = (TextView) findViewById(R.id.tldrTxt);
            TextView longerTxt = (TextView) findViewById(R.id.longerTxt);
            TextView outroTxt = (TextView) findViewById(R.id.outroTxt);
            TextView jokeTxt = (TextView) findViewById(R.id.joke);
            
            String intro  = prefs.getString("intro", "false"); 
            String plus  = prefs.getString("plus", "false"); 
            String name  = prefs.getString("name", "false"); 
            String interval  = prefs.getString("interval", "false"); 
            String switch_var  = prefs.getString("switch_var", "false"); 
            String mute  = prefs.getString("mute", "false"); 
            String vibrate  = prefs.getString("vibrate", "false"); 
            String block  = prefs.getString("block", "false"); 
            String tldr  = prefs.getString("tldr", "false"); 
            String longer  = prefs.getString("longer", "false"); 
            String outro  = prefs.getString("outro", "false"); 
            String joke  = prefs.getString("joke", "false"); 
            
            introTxt.setText(intro);
            plusTxt.setText(plus);
            nameTxt.setText(name);
            intervalTxt.setText(interval);
            switchTxt.setText(switch_var);
            muteTxt.setText(mute);
            vibrateTxt.setText(vibrate);
            blockTxt.setText(block);
            tldrTxt.setText(tldr);
            longerTxt.setText(longer);
            outroTxt.setText(outro);
            jokeTxt.setText(joke);
        }
//        else {
//            jokeTxt = "mata";
//        }
//        joke.setText(jokeTxt);

    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("test", "Setting screen name: helpScreen");
        mTracker.setScreenName("helpScreen");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
//        adapter.notifyDataSetChanged();
    }

}
