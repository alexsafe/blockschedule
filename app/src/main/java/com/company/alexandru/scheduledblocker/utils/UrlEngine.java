package com.company.alexandru.scheduledblocker.utils;

import android.app.Activity;
import android.util.Log;
import android.widget.TextView;

import com.company.alexandru.scheduledblocker.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alexandru on 4/27/2016.
 */
public class UrlEngine {

    private static final int UTF = 0;
    static String baseUrl = "http://findoutaboutnow.net/";
    static String log_tag = "test";
    static String serverCall = baseUrl + "/%method.php?";

    public static boolean getAppData(final Activity activity) {
        StringBuffer link = new StringBuffer();
        String returnStr = "";
        link.append(baseUrl + "app_data.php?");
//        Log.d(log_tag, "getappdata json:" + link);
        JSONClient client = new JSONClient(activity, new JSONClient.GetJSONListener() {
            @Override
            public void onRemoteCallComplete(JSONObject json, boolean success) {
//                Log.d(log_tag, "getappdata json:" + success);
                if (success) {
                    try {
//                        Log.d(log_tag, "getappdata json:" + json);
                        if (json != null) {

//                            String access_token = json.getString("access_token");
//                            String NActions = json.getString("NActions");
//                            String PModal = json.getString("PModal");
//                            String PShare = json.getString("PShare");
//                            String URLtoShare = json.getString("URLtoShare");
//                            String adMobId = json.getString("adMobId");
//                            String ratePackage = json.getString("ratePackage");
//                            String proVersion = json.getString("proVersion");
//                            String PRate = json.getString("PRate");
//                            String timeout = json.getString("timeout");
//                            String help_contents = json.getString("help_contents");
                            String intro = json.getString("intro");
                            String plus = json.getString("plus");
                            String name = json.getString("name");
                            String interval = json.getString("interval");
                            String switch_var = json.getString("switch");
                            String types = json.getString("types");
                            String mute = json.getString("mute");
                            String vibrate = json.getString("vibrate");
                            String block = json.getString("block");
                            String tldr = json.getString("tldr");
                            String longer = json.getString("longer");
                            String outro = json.getString("outro");
                            String joke = json.getString("joke");
                            // returnStr+=access_token+","+NActions+","+PModal+","+PShare+","+URLtoShare;
                            android.content.SharedPreferences.Editor editor = activity.getApplicationContext().getSharedPreferences("ig-user", 0).edit();
                            editor.putString("intro", intro);
                            editor.putString("plus", plus);
                            editor.putString("name", name);
                            editor.putString("interval", interval);
                            editor.putString("switch_var", switch_var);
                            editor.putString("types", types);
                            editor.putString("mute", mute);
                            editor.putString("vibrate", vibrate);
                            editor.putString("block", block);
                            editor.putString("tldr", tldr);
                            editor.putString("longer", longer);
                            editor.putString("outro", outro);
                            editor.putString("joke", joke);
                            editor.putString("in_shared", "true");
                            TextView introTxt = (TextView) activity.findViewById(R.id.intro);
                            TextView plusTxt = (TextView) activity.findViewById(R.id.plus);
                            TextView nameTxt = (TextView) activity.findViewById(R.id.name);
                            TextView intervalTxt = (TextView) activity.findViewById(R.id.intervalTxt);
                            TextView switchTxt = (TextView) activity.findViewById(R.id.switchTxt);
                            TextView muteTxt = (TextView) activity.findViewById(R.id.muteTxt);
                            TextView vibrateTxt = (TextView) activity.findViewById(R.id.vibrateTxt);
                            TextView blockTxt = (TextView) activity.findViewById(R.id.blockTxt);
                            TextView tldrTxt = (TextView) activity.findViewById(R.id.tldrTxt);
                            TextView longerTxt = (TextView) activity.findViewById(R.id.longerTxt);
                            TextView outroTxt = (TextView) activity.findViewById(R.id.outroTxt);
                            TextView jokeTxt = (TextView) activity.findViewById(R.id.joke);
                            introTxt.setText(intro);
                            plusTxt.setText(plus);
                            nameTxt.setText(name);
                            intervalTxt.setText(interval);
                            switchTxt.setText(switch_var);
                            muteTxt.setText(mute);
                            vibrateTxt.setText(vibrate);
                            blockTxt.setText(block);
                            tldrTxt.setText(tldr);
                            longerTxt.setText(longer);
                            outroTxt.setText(outro);
                            jokeTxt.setText(joke);
                            editor.commit();

                        }
                        // LB.mData = mData;
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else if (!success) {
//                    Log.d("test", "error");
//                    M.doErrorCheck(activity, json);
                }
            }

        }, false, "Getting app data...", false, false);
        client.execute(link.toString());
        return true;
    }
}
