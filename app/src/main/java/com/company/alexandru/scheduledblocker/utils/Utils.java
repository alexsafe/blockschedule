package com.company.alexandru.scheduledblocker.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;
import android.util.Xml;

import com.company.alexandru.scheduledblocker.types.ContactInfo;

import org.apache.commons.io.IOUtils;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.company.alexandru.scheduledblocker.utils.AppVariables.dataFile;

/**
 * Created by alexandru on 4/2/2016.
 */
public class Utils {

    public void createFile(Context context, List<ContactInfo> contact, String xmlFile) {
//        final String xmlFile = "userData";
        String userNAme = "username";
        String password = "password";
        FileOutputStream fileos;
//        //Log.d("test","create file:"+xmlFile);
        try {
            fileos = context.openFileOutput(xmlFile, Context.MODE_PRIVATE);
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag(null, "userData");
            if (contact != null)
            {
                for (ContactInfo currentContact : contact) {
                    xmlSerializer.startTag(null, "contact");
//                    xmlSerializer.attribute("", "id", currentContact.getCode());
                    xmlSerializer.attribute("", "id", currentContact.getCode());
                    xmlSerializer.startTag("", "name");
                    xmlSerializer.text(currentContact.getName());
                    xmlSerializer.endTag("", "name");

//                        xmlSerializer.startTag("", "phones");
                    int index = 1;
                    for (String phone : currentContact.getPhone_nos()) {
                        xmlSerializer.startTag("", "phone_" + index);
                        xmlSerializer.text(phone);
                        xmlSerializer.endTag("", "phone_" + index);
                        index++;
                    }
//                        xmlSerializer.endTag("", "phones");

                    xmlSerializer.startTag("", "startTime");
                    xmlSerializer.text(currentContact.getStartHour());
                    xmlSerializer.endTag("", "startTime");
                    xmlSerializer.startTag("", "endTime");
                    xmlSerializer.text(currentContact.getEndHour());
                    xmlSerializer.endTag("", "endTime");
                    xmlSerializer.startTag("", "type");
                    xmlSerializer.text(currentContact.getTypeAction());
                    xmlSerializer.endTag("", "type");
                    xmlSerializer.startTag("", "activated");
                    xmlSerializer.text(currentContact.getActivated());
                    xmlSerializer.endTag("", "activated");
                    xmlSerializer.endTag(null, "contact");
//                    //Log.d("test", "in createfile activated for :" + currentContact.getName() + " is :" + currentContact.getActivated() + " to:" + currentContact.getTypeAction());
                }
            }
            xmlSerializer.endTag(null, "userData");
            xmlSerializer.endDocument();
            xmlSerializer.flush();
            String dataWrite = writer.toString();
            fileos.write(dataWrite.getBytes());
            fileos.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
//            //Log.d("test","exception fnf");
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
//            //Log.d("test","exception IllegalArgumentException");
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
//            //Log.d("test","exception IllegalStateException");
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
//            //Log.d("test","exception IOException");
            e.printStackTrace();
        }
    }

    public void createFileInit(Context context, List<ContactInfo> contact, String xmlFile) {
//        final String xmlFile = "userData";
        String userNAme = "username";
        String password = "password";
        FileOutputStream fileos;
        try {
//            FileOutputStream fos = new  FileOutputStream("userData.xml");
            if (contact == null) {
//                //Log.d("test", "create file in if");
                fileos = context.openFileOutput(xmlFile, Context.MODE_PRIVATE);
            } else {
//                //Log.d("test", "create file in else");
                fileos = context.openFileOutput(xmlFile, Context.MODE_APPEND);
            }
            XmlSerializer xmlSerializer = Xml.newSerializer();
            StringWriter writer = new StringWriter();
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);
            xmlSerializer.startTag(null, "userData");
//            if (contact != null)
            {
//                for (ContactInfo currentContact : contact)
                {
                    xmlSerializer.startTag(null, "contact");
//                    xmlSerializer.attribute("", "id", currentContact.getCode());
                    xmlSerializer.attribute("", "id", "test");
                    xmlSerializer.startTag("", "name");
                    xmlSerializer.text("2");
                    xmlSerializer.endTag("", "name");

//                        xmlSerializer.startTag("", "phones");
                    int index = 1;
//                        for (String phone : currentContact.getPhone_nos())
                    {
                        xmlSerializer.startTag("", "phone_1");
                        xmlSerializer.text("test");
                        xmlSerializer.endTag("", "phone_1");
                        index++;
                    }
//                        xmlSerializer.endTag("", "phones");

                    xmlSerializer.startTag("", "startTime");
                    xmlSerializer.text("test");
                    xmlSerializer.endTag("", "startTime");
                    xmlSerializer.startTag("", "endTime");
                    xmlSerializer.text("test");
                    xmlSerializer.endTag("", "endTime");

                    xmlSerializer.endTag(null, "contact");
                }
            }


//            xmlSerializer.startTag(null,"password");
//            xmlSerializer.text("pisat");
//            xmlSerializer.endTag(null, "password");
            xmlSerializer.endTag(null, "userData");
            xmlSerializer.endDocument();
            xmlSerializer.flush();
            String dataWrite = writer.toString();
            fileos.write(dataWrite.getBytes());
            fileos.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    public String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("userData");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
//                    //Log.d("test", "received string:" + receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
//            //Log.d("test", "File not found: " + e.toString());
        } catch (IOException e) {
//            //Log.d("test", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public boolean fileExistance(String fname, Context context) {
        File file = context.getFileStreamPath(fname);
        return file.exists();
    }


    public String readFromFile(String fileName, Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(fileName);

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
//                    //Log.d("test", "received string:" + receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
//            //Log.d("test", "File not found: " + e.toString());
        } catch (IOException e) {
//            //Log.d("test", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public boolean isMyServiceRunning(Class<?> serviceClass, Context ctx) {
        ActivityManager manager = (ActivityManager) ctx.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<ContactInfo> xmlToList(String appData){
        XmlParser xmlParser = new XmlParser();
        ArrayList<ContactInfo> entries = null;
        InputStream streamData = null;

        try {
            streamData = IOUtils.toInputStream(appData, "UTF-8");
            entries = xmlParser.parse(streamData);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        return entries;
    }

    public void modifyActivated(String code, boolean isChecked, Context context) {
        if (fileExistance(dataFile,context)) {
            String currentData = readFromFile(dataFile, context);
            List<ContactInfo> entries = xmlToList(currentData);
            int i=0;
            for (ContactInfo contact : entries) {
//                //Log.d("test","verify codes:"+contact.getCode()+ " and  "+code);
                if (contact.getCode().equals(code)) {
//                    //Log.d("test", "verify egale pentru " + contact.getName() + " i:" + i);
                    if (isChecked){
                        ////Log.d("test","check id checked");
                        contact.setActivated("true");
                    }
                    else
                    {
//                        //Log.d("test","check id not checked");
                        contact.setActivated("false");
                    }
                    entries.set(i,contact);
                }
                i++;
            }
            for(ContactInfo contactInfo:entries){
//                //Log.d("test","contact "+contactInfo.getName());
//                //Log.d("test", "contact activated "+contactInfo.getActivated());

            }
            createFile(context, entries, dataFile);
        }
    }

    public void modifyHours( Context context,ContactInfo mContact) {
//        //Log.d("test","modifyHours: "+mContact);
        if (fileExistance(dataFile,context)) {
//            //Log.d("test","modifyHours1: ");
            String currentData = readFromFile(dataFile, context);
            List<ContactInfo> entries = xmlToList(currentData);
            int i=0;
//            //Log.d("test","modifyHours2: ");

//            for(ContactInfo contactInfo:entries){
//                //Log.d("test","modifyHours before contact "+contactInfo.getName());
//                //Log.d("test", "modifyHours before contact activated "+contactInfo.getActivated());

//            }
            for (ContactInfo contact : entries) {

                if (contact.getCode().equals(mContact.getCode())) {
//                    //Log.d("test", "modifyHours egale pentru " + contact.getName() + " i:" + i);

//                    contact.setName(mContact.getName());
                    contact.setStartHour(mContact.getStartHour());
                    contact.setEndHour(mContact.getEndHour());
//                    contact.setPhone_nos(mContact.getPhone_nos());
//                    contact.setActivated(mContact.getActivated());
//                    contact.setCode(mContact.getCode());
//                    contact.setTypeAction("edit");

                    entries.set(i,contact);

                }
                i++;
            }

//            for(ContactInfo contactInfo:entries){
//                //Log.d("test","modifyHours after contact "+contactInfo.getName());
//                //Log.d("test", "modifyHours after contact activated "+contactInfo.getActivated());
//
//            }

            createFile(context, entries, dataFile);
        }
    }

    public void deleteContact(String code, Context context) {

        if (fileExistance(dataFile,context)) {
            String currentData = readFromFile(dataFile, context);
            List<ContactInfo> entries = xmlToList(currentData);
            Iterator<ContactInfo> it = entries.iterator();
            while(it.hasNext()){
                ContactInfo object = it.next();
                if (object.getCode().equals(code)) {
                    it.remove();
                }
            }
//            int i=0;
//            for (ContactInfo contact : entries) {
//                //Log.d("test","verify codes:"+contact.getCode()+ " and  "+code);
//                if (contact.getCode().equals(code)) {
//                    //Log.d("test","verify egale pentru "+contact.getName()+" i:"+i);
//
//                    entries.remove(i);
//                }
//                i++;
////            }
//            for(ContactInfo contactInfo:entries){
//                //Log.d("test","indelete contact "+contactInfo.getName());
//
//            }

            createFile(context, entries, dataFile);
        }
    }

    public void modifyActionType( Context context,ContactInfo mContact) {
//        //Log.d("test","modifyActionType: "+mContact);
        if (fileExistance(dataFile,context)) {
//            //Log.d("test","modifyActionType1: ");
            String currentData = readFromFile(dataFile, context);
            List<ContactInfo> entries = xmlToList(currentData);
            int i=0;
//            //Log.d("test","modifyActionType2: ");

//            for(ContactInfo contactInfo:entries){
//                //Log.d("test","modifyActionType before contact "+contactInfo.getName());
//                //Log.d("test", "modifyActionType before contact activated " + contactInfo.getTypeAction());
//
//            }
            for (ContactInfo contact : entries) {
                if (contact.getCode().equals(mContact.getCode())) {
//                    //Log.d("test", "modifyActionType egale pentru " + contact.getName() + " i:" + i);
                    contact.setTypeAction(mContact.getTypeAction());
                    entries.set(i,contact);
                }
                i++;
            }
//            for(ContactInfo contactInfo:entries){
//                //Log.d("test","modifyActionType after contact "+contactInfo.getName());
//                //Log.d("test", "modifyActionType after contact activated " + contactInfo.getTypeAction());
//
//            }
            createFile(context, entries, dataFile);
        }
    }


    public void modifyActionType(String code, String action, Context context) {
        if (fileExistance(dataFile, context)) {
            String currentData = readFromFile(dataFile, context);
            List<ContactInfo> entries = xmlToList(currentData);
            int i = 0;
            for (ContactInfo contact : entries) {
//                //Log.d("test", "modifyActionType verify codes:" + contact.getCode() + " and  " + code);
                if (contact.getCode().equals(code)) {
//                    //Log.d("test", "modifyActionType verify egale pentru " + contact.getName() + " i:" + i);
                    contact.setTypeAction(action);
                    entries.set(i, contact);
                }
                i++;
            }
//            for (ContactInfo contactInfo : entries) {
//                //Log.d("test", "modifyActionType contact " + contactInfo.getName());
//                //Log.d("test", "modifyActionType contact activated " + contactInfo.getTypeAction());
//
//            }
            createFile(context, entries, dataFile);
        }
    }
}
