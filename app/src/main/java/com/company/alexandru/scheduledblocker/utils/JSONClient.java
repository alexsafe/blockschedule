package com.company.alexandru.scheduledblocker.utils;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class JSONClient extends AsyncTask<String, Void, JSONObject> {
    AlertDialog progressDialog;
    GetJSONListener getJSONListener;
    Activity curContext;
    boolean result, blurryBackground;
    String loadingDialog;
    boolean showDialog;
    boolean showHart = false;
    String log_tag = "test";
    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (msg.what == 0) {
                if (curContext != null) {
                    progressDialog = new ProgressDialog(curContext);
                    progressDialog.setCancelable(true);
                    progressDialog.show();

                    if (!showHart)
                        progressDialog.setMessage(loadingDialog);

                    if (showHart) {
                        Log.d("test", "loading");
//						progressDialog.setContentView(R.layout.widget_loading_dialog);
                    }
                }

            }

            if (msg.what == 1) {
                if (progressDialog != null) {
                    if (progressDialog.isShowing()) {
                        try {
                            progressDialog.dismiss();
                            progressDialog = null;
                        } catch (Exception e) {
                            Log.d(log_tag, "dialog exception:" + e);
                        }
                    }
                    // progressDialog.dismiss();
                }
            }

        }
    };

    public JSONClient(Activity context, GetJSONListener listener, boolean blurryBackground, String loadingDialog, boolean showHart, boolean showDialog) {
        this.getJSONListener = listener;
        this.curContext = context;
        this.blurryBackground = blurryBackground;
        this.loadingDialog = loadingDialog;
        this.showDialog = showDialog;
        this.showHart = showHart;

    }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            // Log.d(log_tag, "convertStreamToString IOException:" + e);
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                // Log.d(log_tag, "convertStreamToString finally IOException:" +
                // e);
                e.printStackTrace();
            }
        }
        // Log.d(log_tag, "json sbtostring:" + sb.toString());

        return sb.toString();
    }

    public JSONObject connect(String url) {
        // HttpClient httpclient = new DefaultHttpClient();
        HttpParams httpParameters = new BasicHttpParams();
        SharedPreferences sharedpreferences = curContext.getSharedPreferences("ig-user", 0);
        String timeout = sharedpreferences.getString("timeout", "14");
//        Log.d(log_tag, "timeout in json client:" + timeout);
        int timeoutConnection = Integer.parseInt(timeout) * 1000;
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        int timeoutSocket = Integer.parseInt(timeout) * 1000;
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        HttpClient httpclient = new DefaultHttpClient(httpParameters);
        HttpGet httpget = new HttpGet(url);
//        Log.d("JSONClient url", url);
        // Execute the request
        HttpResponse response = null;

        try {
            response = httpclient.execute(httpget);

            // Examine the response status
            Log.i("Praeda", response.getStatusLine().toString());

            // Get hold of the response entity
            HttpEntity entity = response.getEntity();

            if (entity != null) {

                // A Simple JSON Response Read
                InputStream instream = entity.getContent();
                // Log.d(log_tag, "instreamtoStringlength:" +
                // instream.toString());
                // Log.d(log_tag, "instreamtoStringlength:" +
                // instream.toString().length());
                String result = convertStreamToString(instream);
//                Log.d("JSONClient " + url, result);
                // A Simple JSONObject Creation
                JSONObject json = new JSONObject(result);

                // Closing the input stream will trigger connection release
                instream.close();

                return json;
            }

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
//            Log.d(log_tag, "JSONClient ClientProtocolException: " + e);
            e.printStackTrace();
            cancel(true);
        } catch (IOException e) {
            // TODO Auto-generated catch block
//            Log.d(log_tag, "JSONClient IOException: " + e);
            e.printStackTrace();
            cancel(true);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
//            Log.d(log_tag, "JSONClient JSONException: " + e);
            e.printStackTrace();
            cancel(true);
        } catch (Exception m) {
//            Log.d(log_tag, "JSONClient Exception: " + m);
            m.printStackTrace();
            cancel(true);
        }

        return null;
    }

    @Override
    public void onPreExecute() {
        // progressDialog = new ProgressDialog(curContext);
        // progressDialog.setMessage("iau muie asteptati");
        // progressDialog.setCancelable(true);
        // progressDialog.show();
//        Log.d(log_tag, "jsonclient preexecute");
        if (showDialog)
            handler.sendEmptyMessage(0);
    }

    @Override
    protected JSONObject doInBackground(String... urls) {
        try {
//            Log.d(log_tag, "jsonclient doInBackground");
            return connect(urls[0]);
        } catch (Exception m) {
            m.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(JSONObject json) {

        handler.sendEmptyMessage(1);
//        Log.d(log_tag, "jsonclient onPostExecute : " + json);
//        Log.d(log_tag, "jsonclient onPostExecute getJSONListener: " + getJSONListener);
        if (getJSONListener != null)
            if (json != null)
                getJSONListener.onRemoteCallComplete(json, true);

    }

    protected void onCancelled() {
//        Log.d(log_tag, "JSONClient2 canceled");
        if (progressDialog != null)
            progressDialog.dismiss();
        getJSONListener.onRemoteCallComplete(null, false);
    }

    public interface GetJSONListener {
        void onRemoteCallComplete(JSONObject jsonFromNet, boolean result);
    }
}